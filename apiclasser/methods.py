import javalang


def get_all_methods(java_code):
    s = set()
    tree = javalang.parse.parse(java_code)
    for path, method in tree.filter(javalang.tree.MethodDeclaration):
        success = True
        for c in path:
            if isinstance(c, javalang.tree.ClassDeclaration) and 'public' not in c.modifiers:
                success = False
                break
        if not success:
            continue
        anns = [a.name for a in method.annotations]
        if 'public' not in method.modifiers and 'Override' not in anns:
            continue
        s.add(method.name)
    return s
