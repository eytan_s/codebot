from bluemix import db_connection
from apiclasser import methods
import javalang
import re
from collections import namedtuple
import pymongo


def add_classification(version, func):
    collection = db_connection.sof_collection

    def update_from_dict(data):
        for k in data.keys():
            if len(data[k]) > 0:
                collection.update_many({'_id': {'$in': data[k]}}, {'$set': {'version' + str(version): k}})
            data[k] = []

    update_queue = {}
    rounds = 0
    for doc in db_connection.get_sof():
        res = func(doc)
        if res not in update_queue:
            update_queue[res] = []
        update_queue[res].append(doc._id)
        rounds += 1
        if rounds > 10000:
            update_from_dict(update_queue)

    update_from_dict(update_queue)


class ByMethodCalls(object):
    API = namedtuple('API', ['name', 'code'])

    class Method(object):
        def __init__(self, name):
            self.name = name

    def __init__(self):
        # Collect all apis to methods
        self.apis = {}
        for name, code in db_connection.get_apis_code():
            try:
                ms = methods.get_all_methods(code)
                for m in ms:
                    self.apis[m] = ByMethodCalls.API(name=name, code=code)
            except:
                pass

    def classify(self, tested_doc):
        try:
            t = javalang.parse.parse(tested_doc.sourceCode)
            tested_calls = [mi.member for mi in t.filter(javalang.tree.MethodInvocation)]
            for m in tested_calls:
                if m in self.apis:
                    return self.apis[m].name
        except:
            function_calls = re.findall(r'(?:\s|\A|\.)([a-zA-Z_][a-zA-Z_0-9]*)\s*\(', tested_doc.sourceCode)
            for m in function_calls:
                if m in self.apis:
                    return self.apis[m].name
        return None
