import telepot
import json
import shelve
from datetime import datetime
from watson_developer_cloud import ConversationV1

from message import Message


class IbmHandler(object):
    def __init__(self, bot, timeout=3600):
        self.context_timeout=timeout
        self.bot = bot
        self.workspace_id = "0fcdf441-7457-4dd0-afc1-bcbf0935b9b9"
        self.credentials = {
          "credentials": {
            "url": "https://gateway.watsonplatform.net/conversation/api",
            "password": "qkvCbLanuGPo",
            "username": "99b5c39c-bf90-47d3-96aa-834972e610e1"
          }
        }

        self.conversation = ConversationV1(
          username=self.credentials['credentials']['username'],
          password=self.credentials['credentials']['password'],
          version='2016-07-11'
        )

        self.contexts = {}

    def ibm_handler(self, msg):
        msg = Message(msg)
        context = {}
        if msg.chat_id in self.contexts:
            old_context, time = self.contexts[msg.chat_id]
            if abs((datetime.now() - time).total_seconds()) < self.context_timeout:
                context = old_context
        response = self.conversation.message(workspace_id=self.workspace_id, message_input={'text': msg.text}, context=context)
        self.bot.sendMessage(msg.chat_id, json.dumps(response, indent=2))


def main():
    bot = telepot.Bot("234509651:AAEpvvos5b2DYfwUWu3e4aRj41ChMS_p5W8")
    handler = IbmHandler(bot)
    bot.message_loop(handler.ibm_handler, run_forever=True)


if __name__ == '__main__':
    main()
