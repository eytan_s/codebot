import telepot
import io


class Message(object):
    def __init__(self, msg, file_getter=None):
        self.msg = msg
        self.result = None
        self.content_type, self.chat_type, self.chat_id = telepot.glance(msg)
        self.chat_id = self.chat_id
        self.file = None
        self.file_getter = file_getter
        self.auth = False
        self.user_id = None
        self.code = None
        if self.content_type == 'document':
            self.file_id = msg['document']['file_id']
            self.file_name = msg['document']['file_name']
            self.file_size = msg['document']['file_size']
        elif self.content_type == 'text':
            self.text = msg['text']

    def __delitem__(self, key):
        self.msg.__delitem__(key)

    def __getitem__(self, key):
        return self.msg[key]

    def __setitem__(self, key, value):
        self.msg[key] = value

    def __contains__(self, item):
        return item in self.msg

    def __str__(self):
        return str(self.msg)

    def is_command(self):
        return self.content_type == 'text' and self.text.startswith('/')

    def is_file(self):
        return self.content_type == 'document' or self.content_type == 'photo'

    def get_file(self):
        if not self.is_file():
            return None
        if self.file is None:
            self.file = io.BytesIO()
            self.file_getter(self.file_id, self.file)
        self.file.seek(0)
        return self.file

    def authenticated(self):
        return self.auth
