package com.parsers.sof.java;

import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;

public class Ast {
	private class Visitor extends ASTVisitor {
		public boolean visit(VariableDeclarationFragment node) {
			SimpleName name = node.getName();
			String sName = name.getIdentifier();
			return false; // do not continue to avoid usage info
		}

		public boolean visit(SimpleName node) {
			return true;
		}
	}
	
	public Ast(String code) {
		ASTParser parser = ASTParser.newParser(AST.JLS3);
		parser.setSource(code.toCharArray());
		// There are four kinds of units. need to try them all and handle methods by declaring empty classes.
		// TODO: add support for other types
		parser.setKind(ASTParser.K_STATEMENTS);
		ASTNode baseNode = parser.createAST(null);
		Block block = (Block) baseNode;
		block.accept(new Visitor());
	}
}
