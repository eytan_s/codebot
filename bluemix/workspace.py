import json
import os
import csv
import shutil

from io import StringIO

from bluemix import utilities
from bluemix import db_connection

BLUEMIX_LIMIT = 20000


class BaseJsonable(object):
    def json(self):
        return self.__dict__


class Example(BaseJsonable):
    def __init__(self, text):
        self.created = utilities.time_to_bluemix_format()
        self.text = text


class Intent(object):
    def __init__(self, intent, examples, description=None, created=None):
        self.intent = intent
        self.examples = examples
        self.created = created
        if created is None:
            self.created = utilities.time_to_bluemix_format()
        self.description = description

    def json(self):
        examples = []
        for example in self.examples:
            if not isinstance(example, dict):
                example = example.json()
            examples.append(example)
        self.examples = examples
        return self.__dict__


class Value(BaseJsonable):
    def __init__(self, value, synonyms, metadata=None):
        self.value = value
        self.created = utilities.time_to_bluemix_format()
        self.metadata = metadata
        self.synonyms = synonyms  # holds string values


class Entity(BaseJsonable):
    def __init__(self, entity, values, open_list=False, description=None, created=None):
        self.description = description
        if created is None:
            self.created = utilities.time_to_bluemix_format()
        self.entity = entity
        self.open_list = open_list
        self.values = values


class DialogNode(BaseJsonable):
    def __init__(self, dialog_node, conditions, previous_sibling=None, go_to=None, output=None, parent=None,
                 description=None, context=None, metadata=None, created=None):
        self.conditions = conditions
        self.previous_sibling = previous_sibling
        self.go_to = go_to
        self.output = output
        self.dialog_node = dialog_node
        self.parent = parent
        self.description = description
        self.metadata = metadata
        self.context = context
        self.created = created
        if created is None:
            self.created = utilities.time_to_bluemix_format()


class JsonWorkspace(object):
    def __init__(self, old):
        if isinstance(old, str):
            self.workspace = json.loads(old)
        self.dialog_nodes = [DialogNode(**dn) for dn in self.dialog_nodes]
        self.intents = [Intent(**i) for i in self.intents]
        self.entities = [Entity(**e) for e in self.entities]
        self.workspace['dialog_nodes'] = self.dialog_nodes
        self.workspace['intents'] = self.intents
        self.workspace['entities'] = self.entities

    def __getattr__(self, name):
        try:
            return self.workspace[name]
        except KeyError as e:
            raise AttributeError("Child' object has no attribute '%s'" % name)

    def json(self):
        temp = self.workspace.copy()
        temp['dialog_nodes'] = []
        for n in self.workspace['dialog_nodes']:
            temp['dialog_nodes'].append(n.json())
        temp['intents'] = []
        for n in self.workspace['intents']:
            temp['intents'].append(n.json())
        temp['entities'] = []
        for n in self.workspace['entities']:
            temp['entities'].append(n.json())
        return temp

    def __str__(self):
        return json.dumps(self.json(), indent=4)

    def no_samples(self):
        temp = self.json()
        del temp['intents']
        return temp

    def samples_as_csv(self):
        # fix rows with more then 2 items - problem was in calc using semicolon as seperator
        temp = self.json()
        intents = temp['intents']
        results = []
        out = None
        rows_count = 0
        intent_count = 0
        found_vals = set()
        for i in intents:
            intent_count += 1
            results.append(StringIO(newline=''))
            out = csv.writer(results[-1])
            rows_count = 0
            for e in i['examples']:
                if (e['text'], i['intent']) not in found_vals:
                    found_vals.add((e['text'], i['intent']))
                    out.writerow([e['text'], i['intent']])
                    rows_count += 1
                    if rows_count >= BLUEMIX_LIMIT:
                        break
        print("intent count is: {}".format(intent_count))
        print("rows count is: {}".format(rows_count))
        return results


    # self.created_by = old['created_by']
    # self.modified = old['modified']
    # self.created = old['created']
    # self.language = "en"
    # self.description = "Auto created workspace for code bot"
    # self.workspace_id = old['workspace_id']
    # self.modified_by = None
    # self.name = old['name']
    # self.metadata = None


class MongoWorkspace(JsonWorkspace):
    def __init__(self, version):
        current_time = utilities.time_to_bluemix_format()
        # Missing the following: intents, entities and dialog_nodes
        self.workspace = {
            "created_by": None,
            "modified": current_time,
            "created": current_time,
            "language": "en",
            "description": "Auto created workspace from mongodb at " + current_time,
            "workspace_id": "c505efe6-81d8-4eea-b496-bf6306a2eb66",
            "modified_by": None,
            "name": "CodeBot V" + str(version),
            "metadata": None,
            "entities": [],
            'intents': [],
            "dialog_nodes": []
        }
        super(MongoWorkspace, self).__init__(self.workspace)

        # Create intents
        intent_values = db_connection.sof_collection.distinct('version' + str(version), {})
        if None in intent_values:
            intent_values.remove(None)
        results = filter(lambda val: db_connection.sof_collection.find({'version' + str(version): val}).count() > 5,
                         intent_values)
        self.workspace['intents'] = []
        for intent in results:
            examples = []
            for doc in db_connection.sof_collection.find({'version' + str(version): intent}):
                examples.append(Example(doc['sofinfo']['title']))
            self.workspace['intents'].append(Intent(intent, examples))

        # Create dialog
        self.workspace["dialog_nodes"] = [
            # DialogNode({
            #     "output": {'text': 'last node'},
            #     "previous_sibling": None,
            #     "dialog_node": "node_3_1472423640260",
            #     "conditions": "anything_else",
            #     "go_to": None,
            #     "metadata": None,
            #     "parent": None,
            #     "context": None,
            #     "created": "2016-08-28T22:34:00.561Z",
            #     "description": None
            # }, ("anything_else",))
        ]


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('version')
    parser.add_argument('out')
    args = parser.parse_args()
    w = MongoWorkspace(args.version)
    workspace_path = os.path.join(args.out, 'workspace_v' + str(args.version) + '.json')
    with open(workspace_path, 'w') as w_out:
        w_out.write(json.dumps(w.no_samples(), indent=4))
    results = w.samples_as_csv()
    for i in range(len(results)):
        csv_path = os.path.join(args.out, 'intents_v{}_part{}.csv'.format(args.version, str(i)))
        with open(csv_path, 'w') as csv_out:
            buf = results[i]
            buf.seek(0)
            shutil.copyfileobj(buf, csv_out)
