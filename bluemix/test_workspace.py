import os
import json
from unittest import TestCase

from bluemix import workspace


class TestWorkspace(TestCase):
    def setUp(self):
        json_p = os.path.dirname(__file__) + os.sep + 'resources' + os.sep + 'formated_workspace.json'
        self.data = open(json_p).read()
        self.w = workspace.JsonWorkspace(self.data)

    def test_workspace_creation(self):
        self.assertNotEqual(0, len(self.w.dialog_nodes))
        self.assertNotEqual(type(self.w.dialog_nodes[0]), str)

    def test_str_correct(self):
        print(json.loads(str(self.w)))
        self.assertEqual(json.loads(self.data), json.loads(str(self.w)))
