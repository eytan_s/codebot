import time


def time_to_bluemix_format(t=None):
    if t is None:
        t = time.gmtime()
    return time.strftime("%Y-%m-%dT%H-%M-%S.000Z", t)