from unittest import TestCase
import utilities


class TestUtilities(TestCase):
    def test_time_to_bluemix_format(self):
        cur = utilities.time_to_bluemix_format()
        self.assertNotEqual(cur, None)
        self.assertNotEqual(cur.strip(), "")
