import pymongo
import jsonpickle


class Struct(object):
    def __init__(self, collection, **entries):
        self.collection = collection
        self.__dict__.update(entries)

    def update(self, field, val):
        self.collection.update_one({'_id': self._id}, {'$set': {field: val}})


client = pymongo.MongoClient()
db = client.sofapis
sof_collection = db.sof
api_collection = db.api


def __map_with_close(func, iterable, closer):
    try:
        for o in iterable:
            yield func(o)
    finally:
        closer()


def __get_from_container(container):
    curs = container.find({}, no_cursor_timeout=True)
    return __map_with_close(lambda x: Struct(container, **x), curs, curs.close)


def get_apis():
    return __get_from_container(api_collection)


def get_sof():
    return __get_from_container(sof_collection)


def get_apis_code():
    return map(lambda doc: (doc.artifactName, doc.sourceCode), get_apis())


